# testing_react

Testing react w docker

Will spin up a nodejs container which will run our react application

*One-time command*, to install dependencies after cloning the repository:

`docker-compose run --rm app npm install`

To run the container:

`docker-compose up -d`

To login into the container

`docker-compose exec app sh`